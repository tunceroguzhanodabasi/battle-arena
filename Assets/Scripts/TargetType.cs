﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetDirection
{
    Enemy,
    Friendly,
    All
}

[Serializable]
public class TargetType
{
    public TargetDirection TargetDirection;

    public bool İsUserİnclude;

    public int Size;

    public bool İsRandom;
}