﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {

    public int Id;

    public int Health = 100;

    public bool İsDead = false;

    public void takeDamage(int amount)
    {
        Health -= amount;
        if (Health < 0)
        {
            Health = 0;
            İsDead = true;
        }

        Transform healthT = this.transform.Find("HealthBar").Find("Health").transform;
        healthT.localScale = new Vector3(Health / (100f), healthT.localScale.y, healthT.localScale.z);

        Debug.Log("Health " + Health);
    }

    public void chooseMove(int index)
    {
        GameObject skillUse = this.transform.Find("SkillUse").gameObject;
        GameObject usedSkill = this.transform.Find("Skill" + index).gameObject;

        skillUse.GetComponent<Image>().sprite = usedSkill.GetComponent<Image>().sprite;
        skillUse.GetComponent<Button>().interactable = true;

        for (int i = 1; i <= 4; i++)
        {
            GameObject skill = this.transform.Find("Skill" + i).gameObject;
            skill.GetComponent<Button>().interactable = false;
        }
    }

    public void resetMoves()
    {
        GameObject skillUse = this.transform.Find("SkillUse").gameObject;
        skillUse.GetComponent<Button>().interactable = false;

        for (int i = 1; i <= 4; i++)
        {
            GameObject skill = this.transform.Find("Skill" + i).gameObject;
            skill.GetComponent<Button>().interactable = true;
        }
    }

    public void closeAll()
    {
        GameObject skillUse = this.transform.Find("SkillUse").gameObject;
        skillUse.GetComponent<Button>().interactable = false;

        for (int i = 1; i <= 4; i++)
        {
            GameObject skill = this.transform.Find("Skill" + i).gameObject;
            skill.GetComponent<Button>().interactable = false;
        }
    }
    public void targetUp()
    {
        this.transform.Find("Target").gameObject.GetComponent<Button>().interactable = true;
    }

    public void targetDown()
    {
        this.transform.Find("Target").gameObject.GetComponent<Button>().interactable = false;
    }
}
