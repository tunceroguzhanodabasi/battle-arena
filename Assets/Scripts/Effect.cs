﻿using System;
using System.Collections.Generic;

[Serializable]
public class Effect {

    public EffectType EffectType;

    public TargetType TargetType;

    public List<ChooseCharVo> Targets;

    public int Amount;

    public int TurnCooldown;

    public int AfterTurn;
 
}
