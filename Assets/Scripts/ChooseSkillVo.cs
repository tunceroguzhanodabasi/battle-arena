﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChooseSkillVo {

    public int CharId;

    public int SkillId;
}
