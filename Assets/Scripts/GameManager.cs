﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    
    public Dictionary<int, GameObject> Team;

    public Dictionary<int, GameObject> EnemyTeam;

    public Dictionary<int, Skill> ChoosedSkills;

    public Dictionary<int, List<Effect>> Turns;

    public int TurnId;

    public Dictionary<int, Dictionary<int,Skill>> TeamSkills;

    private int ChooseTargetSize;
    
    private Effect ChoosenEffect;

    private ChooseSkillVo ChoosenSkill;

    private Dictionary<int,ChooseCharVo> ChoosenChoosableChars;

    //public Dictionary<Energy, int> Energies;

    void Start()
    {
        ChoosedSkills = new Dictionary<int, Skill>();
        TeamSkills = new Dictionary<int, Dictionary<int, Skill>>();

        Turns = new Dictionary<int, List<Effect>>();
        for (int i = 1; i < 31; i++)
            Turns.Add(i,new List<Effect>());
        TurnId = 1;

        ChoosenChoosableChars = new Dictionary<int, ChooseCharVo>();

        TeamSkills.Add(1, new Dictionary<int, Skill>());
        TeamSkills.Add(2, new Dictionary<int, Skill>());
        TeamSkills.Add(3, new Dictionary<int, Skill>());

        List<Effect> effects = new List<Effect>();
        effects.Add(setupEffect(0,5, TargetDirection.Enemy, false, 3, false));
        effects.Add(setupEffect(1,5, TargetDirection.Enemy, false, 3, false));
        TeamSkills[1].Add(1, setupCharSkills(1, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,10, TargetDirection.Enemy, false, 2, false));
        TeamSkills[1].Add(2, setupCharSkills(2, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,15, TargetDirection.Enemy, false, 1, false));
        TeamSkills[1].Add(3, setupCharSkills(3, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,20, TargetDirection.Enemy, false, 1, true));
        TeamSkills[1].Add(4, setupCharSkills(4, effects));

        effects = new List<Effect>();
        effects.Add(setupEffect(0,5, TargetDirection.Enemy, false, 3, false));
        effects.Add(setupEffect(1, 5, TargetDirection.Enemy, false, 3, false));
        TeamSkills[2].Add(1, setupCharSkills(1, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,10, TargetDirection.Enemy, false, 2, false));
        TeamSkills[2].Add(2, setupCharSkills(2, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,15, TargetDirection.Enemy, false, 1, false));
        TeamSkills[2].Add(3, setupCharSkills(3, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,20, TargetDirection.Enemy, false, 1, true));
        TeamSkills[2].Add(4, setupCharSkills(4, effects));

        effects = new List<Effect>();
        effects.Add(setupEffect(1, 5, TargetDirection.Enemy, false, 3, false));
        effects.Add(setupEffect(0,5, TargetDirection.Enemy, false, 3, false));
        TeamSkills[3].Add(1, setupCharSkills(1, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,10, TargetDirection.Enemy, false, 2, false));
        TeamSkills[3].Add(2, setupCharSkills(2, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,15, TargetDirection.Enemy, false, 1, false));
        TeamSkills[3].Add(3, setupCharSkills(3, effects));
        effects = new List<Effect>();
        effects.Add(setupEffect(0,20, TargetDirection.Enemy, false, 1, true));
        TeamSkills[3].Add(4, setupCharSkills(4, effects));

        Team = new Dictionary<int, GameObject>();
        EnemyTeam = new Dictionary<int, GameObject>();

        Team.Add(1,GameObject.FindWithTag("Char1"));
        Team.Add(2, GameObject.FindWithTag("Char2"));
        Team.Add(3, GameObject.FindWithTag("Char3"));

        EnemyTeam.Add(1, GameObject.FindWithTag("EnemyChar1"));
        EnemyTeam.Add(2, GameObject.FindWithTag("EnemyChar2"));
        EnemyTeam.Add(3, GameObject.FindWithTag("EnemyChar3"));

    }

    private Skill setupCharSkills(int v1, object v2)
    {
        throw new NotImplementedException();
    }

    private Effect setupEffect(int afterTurn,int amount, TargetDirection targetDirection, bool isUserİnclude, int size, bool isRandom)
    {
        Effect effect = new Effect();
        effect.AfterTurn = afterTurn;
        effect.Amount = amount;
        effect.EffectType = EffectType.Damage;
        effect.Targets = new List<ChooseCharVo>();
        effect.TargetType = new TargetType();
        effect.TargetType.TargetDirection = targetDirection;
        effect.TargetType.İsUserİnclude = isUserİnclude;
        effect.TargetType.Size = size;
        effect.TargetType.İsRandom = isRandom;

        return effect;
    }

    private Skill setupCharSkills(int id,List<Effect> effects)
    {
        Skill skill = new Skill();
        skill.Id = id;
        skill.Effets = new List<Effect>();
        skill.Effets = effects;
        //skill.Energys = new List<Energy>();
        return skill;
    }

    public void chooseMove(string input)
    {
        ChooseSkillVo chooseSkillVo = JsonUtility.FromJson<ChooseSkillVo>(input);
        ChoosenSkill = chooseSkillVo;
        ChoosenChoosableChars.Clear();
        ChoosenChoosableChars.Add(1,new ChooseCharVo(1, false));
        ChoosenChoosableChars.Add(2,new ChooseCharVo(2, false));
        ChoosenChoosableChars.Add(3,new ChooseCharVo(3, false));
        prepareSkill(chooseSkillVo, ChoosenChoosableChars);
        

    }
    private Skill prepareSkill(ChooseSkillVo chooseSkillVo, Dictionary<int,ChooseCharVo> choosableChars)
    {
        Skill skill = TeamSkills[chooseSkillVo.CharId][chooseSkillVo.SkillId];
        bool isComplated = true;

        foreach (Effect effect in skill.Effets)
        {
            if(choosableChars.Count== effect.TargetType.Size)
            {
                targetAll(effect,choosableChars);
            } else if(effect.TargetType.İsRandom == true)
            {
                ChooseTargetSize = effect.TargetType.Size;
                targetRandom(effect, choosableChars);
            }
            else if (effect.TargetType.İsRandom == false)
            {
                isComplated=targetMode(effect, choosableChars);
                if (!isComplated)
                {
                    closeAllSkillExceptOne(chooseSkillVo.CharId);
                    break;
                }
            }
        }
        if(isComplated)
        {
            ChoosedSkills.Add(chooseSkillVo.CharId, skill);
            openSkills();
        }

        return skill;
    }

    private bool targetMode(Effect effect, Dictionary<int, ChooseCharVo> choosableChars)
    {
        if (effect.TargetType.Size == effect.Targets.Count)
            return true;
        ChoosenEffect = effect;
        ChooseTargetSize = effect.TargetType.Size;
        foreach (ChooseCharVo chooseCharVo in choosableChars.Values)
        {
            if (chooseCharVo.İsFriendly)
                Team[chooseCharVo.CharId].GetComponent<Character>().targetUp();
            else
                EnemyTeam[chooseCharVo.CharId].GetComponent<Character>().targetUp();
        }
        return false;
    }
    public void targeted(string input)
    {
        ChooseCharVo chooseCharVo = JsonUtility.FromJson<ChooseCharVo>(input);
        ChooseTargetSize--;
        ChoosenEffect.Targets.Add(chooseCharVo);

        if (ChooseTargetSize!=0)
        {
            if (chooseCharVo.İsFriendly)
                Team[chooseCharVo.CharId].GetComponent<Character>().targetDown();
            else
                EnemyTeam[chooseCharVo.CharId].GetComponent<Character>().targetDown();
        } else
        {
            foreach(GameObject member in Team.Values)
                member.GetComponent<Character>().targetDown();
            foreach (GameObject member in EnemyTeam.Values)
                member.GetComponent<Character>().targetDown();

            prepareSkill(ChoosenSkill, ChoosenChoosableChars);
        }
    }

    private void targetRandom(Effect effect, Dictionary<int, ChooseCharVo> choosableChars)
    {
        for (int i = 0; i < ChooseTargetSize; i++)
        {
            List<int> keys = new List<int>(choosableChars.Keys);
            Utils.Shuffle<int>(keys);
            ChooseCharVo chooseCharVo = new ChooseCharVo(choosableChars[keys[0]].CharId, choosableChars[keys[0]].İsFriendly);
            choosableChars.Remove(keys[0]);

            effect.Targets.Add(chooseCharVo);
        }
    }

    private void targetAll(Effect effect, Dictionary<int,ChooseCharVo> choosableChars)
    {
        foreach( ChooseCharVo chooseCharVo in choosableChars.Values)
        {
            effect.Targets.Add(new ChooseCharVo(chooseCharVo));
        }
    }

    public void turnEnd()
    {
        foreach (KeyValuePair<int, Skill> entry in ChoosedSkills)
        {
            foreach (Effect effect in entry.Value.Effets)
            {
                Effect effectToAdd = new Effect();
                effectToAdd.AfterTurn = effect.AfterTurn;
                effectToAdd.Amount = effect.Amount;
                effectToAdd.EffectType = effect.EffectType;
                effectToAdd.TargetType = effect.TargetType;
                effectToAdd.TurnCooldown = effect.TurnCooldown;
                effectToAdd.Targets = new List<ChooseCharVo>();
                foreach (ChooseCharVo chooseCharVo in effect.Targets)
                    effectToAdd.Targets.Add(chooseCharVo);
                addEffectToTurn(effect.AfterTurn, effectToAdd);
                /*foreach (ChooseCharVo target in effect.Targets)
                {
                    if(!target.İsFriendly)
                    {
                        Character character = EnemyTeam[target.CharId].GetComponent<Character>();
                        character.takeDamage(effect.Amount);
                    }
                    
                }*/
                effect.Targets.Clear();
            }
        }

        foreach (Effect effect in Turns[TurnId])
        {
            foreach (ChooseCharVo target in effect.Targets)
            {
                if (!target.İsFriendly)
                {
                    Character character = EnemyTeam[target.CharId].GetComponent<Character>();
                    character.takeDamage(effect.Amount);
                }
            }
        }

        TurnId++;
        ChoosedSkills.Clear();
    }
    public void addEffectToTurn(int afterTurn,Effect efffect)
    {
        if (afterTurn + TurnId <= 30)
            Turns[afterTurn + TurnId].Add(efffect);
    }
    public void cancelMove(int charId)
    {
        ChoosedSkills.Remove(charId);
    }

    private void closeAllSkillExceptOne(int charId)
    {
        foreach(int charKey in Team.Keys)
        {
            if(charKey!= charId)
            {
                Team[charKey].GetComponent<Character>().closeAll();
            }
        }
    }

    private void openSkills()
    {
        foreach (int charKey in Team.Keys)
        {
            if (ChoosedSkills.ContainsKey(charKey))
                Team[charKey].GetComponent<Character>().chooseMove(ChoosedSkills[charKey].Id);
            else
                Team[charKey].GetComponent<Character>().resetMoves();
        }
    }
    
}