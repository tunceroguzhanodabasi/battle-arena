﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillType
{
    Physics,
    Chakra,
    Mental
}

[Serializable]
public class Skill {

    //public List<Energy> Energys;
    public int Id;

    public List<Effect> Effets;

    public bool İsFriendly;

    //public List<SkillType> skillTypes;
}
