﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChooseCharVo {
    public int CharId;
    public bool İsFriendly;

    public ChooseCharVo(int charId,bool isfriendly)
    {
        CharId = charId;
        İsFriendly = isfriendly;
    }

    public ChooseCharVo(ChooseCharVo chooseCharVo)
    {
        CharId = chooseCharVo.CharId;
        İsFriendly = chooseCharVo.İsFriendly;
    }
   
	
}
